# Mars Photo Stream

Simple photo frame app for showing the latest images received from the NASA Perseverance rover. This app was designed to run on the [AKImart 10.1 Inch Smart WiFi Digital Photo Frame](https://www.amazon.com/dp/B083SH697H), but should run on other picture frames that use the Frameo service/app. These picture frames appear to run a stripped down version of Android, in the case of the AKImart frame it runs version 6.0.1.

More information on this project can be found in this blog post: [Picture Frame Fron Mars](https://www.prbs23.com/blog/posts/picture-frame-from-mars/)

<div align="center">![Mars Photo Stream app running on AKImart Picture Frame](img/photo_frame.jpg)</div>

## Installation
The MarsPhotoStream app can be installed onto the digital picture frame using [ADB](https://developer.android.com/studio/command-line/adb). First, download the latest apk from our [Releases Page](https://gitlab.com/prbs23/mars-photo-stream/-/releases), then install it with the following adb command:
```
# adb install com.prbs23.marsphotostream-v0.1.0.apk
```

The Mars Photo Stream app registers itself as a "home" app, which allows Android to launch the app at startup. If you just install the Mars Photo Stream APK the picture frame will ask on each boot if you want to launch the Frameo app or Mars Photo Stream. If you would rather the picture frame boot directly into the Mars Photo Stream app, you can disable the Frameo "frame" app:
```
# su root
# pm disable net.frameo.frame
# pm hide net.frameo.frame
```

