package com.prbs23.marsphotostream

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar
import androidx.navigation.Navigation
import androidx.preference.Preference
import androidx.preference.PreferenceViewHolder
import androidx.preference.SeekBarPreference
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.google.android.material.textview.MaterialTextView

class NavPreference : Preference {
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?) : super(context)

    override fun onBindViewHolder(holder: PreferenceViewHolder?) {
        super.onBindViewHolder(holder)
        val toolbar : Toolbar = holder?.findViewById(R.id.toolbar) as Toolbar
        toolbar.setNavigationOnClickListener {
            Navigation.findNavController(toolbar).popBackStack()
        }

    }
}

internal class UnitSeekBarPreference : SeekBarPreference {
    var mSeekBarUnitTextView : TextView
    var mSeekBarUnit : String

    @RequiresApi(Build.VERSION_CODES.M)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        val a = context?.obtainStyledAttributes(attrs, R.styleable.UnitSeekBarPreference, defStyleAttr, defStyleRes)
        mSeekBarUnit = a?.getString(R.styleable.UnitSeekBarPreference_unit_string) ?: ""

        mSeekBarUnitTextView = MaterialTextView(context!!)
        mSeekBarUnitTextView.setTextAppearance(android.R.style.TextAppearance_Medium)
        mSeekBarUnitTextView.text = mSeekBarUnit
    }

    @RequiresApi(Build.VERSION_CODES.M)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)

    @RequiresApi(Build.VERSION_CODES.M)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, R.attr.seekBarPreferenceStyle)

    @RequiresApi(Build.VERSION_CODES.M)
    constructor(context: Context?) : this(context, null)

    override fun onBindViewHolder(holder: PreferenceViewHolder?) {
        super.onBindViewHolder(holder)

        // Inject unit text view into layout
        var layout : LinearLayout = holder?.findViewById(R.id.seekbar_value)?.parent as LinearLayout
        var layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutParams.marginStart = 10
        layout.addView(mSeekBarUnitTextView, layoutParams)

    }
}