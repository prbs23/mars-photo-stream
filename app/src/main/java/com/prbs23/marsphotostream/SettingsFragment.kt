package com.prbs23.marsphotostream

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val androidSettingsPref = findPreference<Preference>("android_settings")
        androidSettingsPref?.setOnPreferenceClickListener { _ ->
            startActivity(Intent(Settings.ACTION_SETTINGS))
            true
        }
    }
}