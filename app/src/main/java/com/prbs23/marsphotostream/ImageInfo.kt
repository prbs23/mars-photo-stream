package com.prbs23.marsphotostream

data class ImageInfo(
    val url     : String,
    val title   : String,
    var caption : String
)
