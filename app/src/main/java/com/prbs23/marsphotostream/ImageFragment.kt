package com.prbs23.marsphotostream

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.ActivityOptions
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.animation.LinearInterpolator
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.animation.doOnEnd
import androidx.fragment.app.FragmentContainerView
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import com.android.volley.RequestQueue
import com.android.volley.toolbox.ImageRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONException
import java.util.concurrent.Semaphore
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import com.prbs23.marsphotostream.databinding.FragmentImageBinding
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit
import java.util.Date

class ImageFragment : Fragment() {
    private var _binding: FragmentImageBinding? = null
    lateinit var preferences : SharedPreferences

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    // Volley request queue for handling API requests to NASA
    lateinit var requestQueue : RequestQueue

    // Current image list
    val imagePageCount = 3
    var imageList = listOf<ImageInfo>()
    var imageListMutex = Semaphore(1)
    var imageProgressBarAnimation : ObjectAnimator? = null
    var imageUpdatePaused = false

    // Periodic update handler for updating the list of images
    lateinit var imageListUpdateHandler: Handler
    private  val imageListUpdateTask = object : Runnable {
        @SuppressLint("SimpleDateFormat")
        override fun run() {
            val newImageInfoList = mutableListOf<ImageInfo>()
            val pageRequestCounter = AtomicInteger(0)
            var requestError = AtomicBoolean(false)
            val apiUrl = "https://mars.nasa.gov/rss/api/?feed=raw_images&category=mars2020,ingenuity&feedtype=json&ver=1.2"

            val latestImageReq = JsonObjectRequest(apiUrl + "&num=1&page=0&order=sol+desc", { latestImageResponse ->
                val lastImageTime = SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ").parse(
                    latestImageResponse.getJSONArray("images").getJSONObject(0)
                        .getString("date_taken_utc") + "-0000"
                )
                val getOldImages = TimeUnit.DAYS.convert(
                    Date().time - lastImageTime.time,
                    TimeUnit.MILLISECONDS
                ) > preferences.getInt("old_image_days", 3)
                val pageCount = latestImageResponse.getInt("total_results") / 100
                (0 until imagePageCount).forEach { pageIdx ->
                    var pageNum = pageIdx
                    if (getOldImages && preferences.getBoolean(
                            "en_random_image_on_old",
                            true
                        )
                    ) {
                        pageNum = (0 until pageCount).random()
                    }
                    val req = JsonObjectRequest(
                        "$apiUrl&num=100&page=$pageNum&&order=sol+desc",
                        { response ->
                            try {
                                val imagesJSON = response.getJSONArray("images")
                                (0 until imagesJSON.length()).forEach {
                                    val imageJSON = imagesJSON.getJSONObject(it)
                                    newImageInfoList.add(
                                        ImageInfo(
                                            url = imageJSON.getJSONObject("image_files")
                                                .getString("large"),
                                            title = imageJSON.getString("title"),
                                            caption = imageJSON.getString("caption")
                                        )
                                    )
                                }
                            } catch (e: JSONException) {
                                e.printStackTrace();
                                requestError.set(true)
                            }
                        },
                        { error ->
                            error.printStackTrace()
                            requestError.set(true)
                        })
                    pageRequestCounter.incrementAndGet()
                    requestQueue.add(req)
                }

                // Once all requests have completed update the imageList list
                lateinit var allRequestsComplete: RequestQueue.RequestEventListener
                allRequestsComplete = RequestQueue.RequestEventListener { _, event ->
                    if (event == RequestQueue.RequestEvent.REQUEST_FINISHED) {
                        pageRequestCounter.decrementAndGet()
                        if (pageRequestCounter.get() == 0) {
                            // Remove this event listener
                            requestQueue.removeRequestEventListener(allRequestsComplete)

                            if (requestError.get()) {
                                // Don't update the image list, just schedule another update in 1 minute
                                imageListUpdateHandler.postDelayed(this, 60 * 1000)
                            } else {
                                // Get image list lock while updating
                                imageListMutex.acquire()
                                imageList = newImageInfoList
                                imageListMutex.release()

                                // Schedule next image list update
                                imageListUpdateHandler.postDelayed(
                                    this,
                                    preferences.getInt("image_list_update_period", 60)
                                        .toLong() * 60 * 1000
                                )
                            }
                        }
                    }
                }
                requestQueue.addRequestEventListener(allRequestsComplete)
            }, { error ->
                error.printStackTrace()
                // Don't update the image list, just schedule another update in 1 minute
                imageListUpdateHandler.postDelayed(this, 60 * 1000)
            })
            requestQueue.add(latestImageReq)
        }
    }

    lateinit var imageUpdateHandler: Handler
    private  val imageUpdateTask = object : Runnable {
        override fun run() {
            imageListMutex.acquire()
            val newImageInfo = imageList.randomOrNull()
            imageListMutex.release()

            if (newImageInfo == null) {
                // If there were no images in the image list, then just show the default image
                binding.imageView.setImageResource(R.drawable.mars)
                binding.titleTextView.text = "Loading Image..."
                binding.captionTextView.text = ""
                // Only wait for a second before trying again
                imageUpdateHandler.postDelayed(this, 1000)
            } else {
                // Download the image
                val req = ImageRequest(newImageInfo.url,
                    { response ->
                        val imageUpdatePeriod = preferences.getInt("image_update_period", 60).toLong()
                        binding.imageView.setImageDrawable(BitmapDrawable(resources, response))
                        binding.titleTextView.text = newImageInfo.title
                        binding.captionTextView.text = newImageInfo.caption
                        imageUpdateHandler.postDelayed(this, imageUpdatePeriod * 60 * 1000)
                        binding.imageChangeProgressBar.progress = 0
                        imageProgressBarAnimation?.cancel()
                        imageProgressBarAnimation = ObjectAnimator.ofInt(binding.imageChangeProgressBar, "progress", binding.imageChangeProgressBar.max)
                        imageProgressBarAnimation?.setDuration(imageUpdatePeriod * 60 * 1000)
                        imageProgressBarAnimation?.setInterpolator(LinearInterpolator())
                        imageProgressBarAnimation?.doOnEnd { imageProgressBarAnimation = null }
                        imageProgressBarAnimation?.start()
                    },
                    0, 0, ImageView.ScaleType.CENTER_INSIDE, Bitmap.Config.ALPHA_8,
                    { error ->
                        error.printStackTrace()
                        imageUpdateHandler.post(this)
                    })
                requestQueue.add(req)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentImageBinding.inflate(inflater, container, false)
        return binding.root

    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        preferences = PreferenceManager.getDefaultSharedPreferences(context)
        requestQueue = Volley.newRequestQueue(context)
        imageListUpdateHandler = Handler(Looper.getMainLooper())
        imageUpdateHandler     = Handler(Looper.getMainLooper())

        // Configure imageView factory
        binding.imageView.setFactory {
            val v = ImageView(context)
            v.scaleType = ImageView.ScaleType.FIT_CENTER
            v.layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
            v
        }

        // Configure infoDraw show animation
        binding.imageView.setOnClickListener {
            if (binding.infoDrawer.translationY == 0f) {
                binding.infoDrawer.animate().translationY(binding.infoDrawer.height.toFloat())
            } else {
                binding.infoDrawer.animate().translationY(0f)
            }
        }

        // Switch to settings fragment when settings button is pressed
        binding.SettingsButton.setOnClickListener { _ ->
            findNavController().navigate(R.id.action_imageFragment_to_settingsFragment)
        }

        // When pause button is pressed, stop image update handler
        binding.pauseImageButton.setOnClickListener {
            if (imageUpdatePaused) {
                // Figure out how much longer was left on the image
                val imageUpdatePeriod = ((preferences.getInt("image_update_period", 60).toFloat() * 60.0 * 1000.0 * (binding.imageChangeProgressBar.max - binding.imageChangeProgressBar.progress)) / binding.imageChangeProgressBar.max).toLong()

                // Restart the image update callback
                imageUpdateHandler.postDelayed(imageUpdateTask, imageUpdatePeriod)

                // Restart the progress bar animation
                imageProgressBarAnimation = ObjectAnimator.ofInt(binding.imageChangeProgressBar, "progress", 10000)
                imageProgressBarAnimation?.setDuration(imageUpdatePeriod)
                imageProgressBarAnimation?.setInterpolator(LinearInterpolator())
                imageProgressBarAnimation?.doOnEnd { imageProgressBarAnimation = null }
                imageProgressBarAnimation?.start()

                // Update paused condition
                imageUpdatePaused = false
                binding.pauseImageButton.setImageResource(android.R.drawable.ic_media_pause)
            } else {
                // Stop the image update callback, this means we won't change images automatically
                imageUpdateHandler.removeCallbacks(imageUpdateTask)

                // Stop the progress bar animation
                imageProgressBarAnimation?.cancel()

                // Update paused condition
                imageUpdatePaused = true
                binding.pauseImageButton.setImageResource(android.R.drawable.ic_media_play)
            }
        }

        // When next image button is pressed, post the image update handler
        binding.nextImageButton.setOnClickListener {
            imageUpdateHandler.removeCallbacks(imageUpdateTask)
            imageUpdateHandler.post(imageUpdateTask)
            imageUpdatePaused = false
            binding.pauseImageButton.setImageResource(android.R.drawable.ic_media_pause)
        }
    }

    @Suppress("InlinedApi")
    override fun onResume() {
        super.onResume()

        imageListUpdateHandler.post(imageListUpdateTask) // Restart image list update handler when app comes to foreground
        if (!imageUpdatePaused) {
            imageUpdateHandler.post(imageUpdateTask) // Start updating images when resumed
        }

        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        val flags =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        activity?.window?.decorView?.systemUiVisibility = flags
        (activity as? AppCompatActivity)?.supportActionBar?.hide()
    }

    override fun onPause() {
        super.onPause()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // Clear the systemUiVisibility flag
        activity?.window?.decorView?.systemUiVisibility = 0

        imageListUpdateHandler.removeCallbacks(imageListUpdateTask) // Don't bother updating image list while app is backgrounded
        imageUpdateHandler.removeCallbacks(imageUpdateTask) // Don't need to update image when paused
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}